# Do Factory patterns
Implementação dos padrões de projeto do site [Do Factory](https://www.dofactory.com/net/design-patterns).

## Creational Patterns

### Singletone

```
$ ./gradlew creational-singletone:run
Objects are the same instance
```


