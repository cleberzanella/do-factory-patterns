package com.atmosquare.dofactorypatterns.creational.singletone;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @see <a href="https://www.dofactory.com/net/singleton-design-pattern">Do Factory Singletone</a>
 */
public class Main {


    public static void main(String[] args) throws IOException {

        // Constructor is protected -- cannot use new
        Singleton s1 = Singleton.getInstance();
        Singleton s2 = Singleton.getInstance();

        // Test for same instance
        if (s1 == s2) {
            System.out.println("Objects are the same instance");
        }

        // Wait for user
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        br.readLine();

    }

    /**
     * The 'Singleton' class
     */
    static class Singleton {

        private static Singleton INSTANCE;

        // Constructor is 'protected'
        protected Singleton() {

        }

        public static Singleton getInstance() {

            // Uses lazy initialization.
            // Note: this is not thread safe.
            if (INSTANCE == null) {
                INSTANCE = new Singleton();
            }

            return INSTANCE;
        }

    }

}

